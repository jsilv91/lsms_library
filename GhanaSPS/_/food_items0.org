#+title: Food_items0

| 1988                                                         | 1989                                                         | 1991                                 | 1998 | Preferred Label |
|--------------------------------------------------------------+--------------------------------------------------------------+--------------------------------------+------+-----------------|
| Rice                                                         | Rice                                                         | Rice                                 |      |                 |
| Maize (cob, grain, dough or flour)                           | Maize (cob, grain, dough or flour)                           | Maize                                |      |                 |
|                                                              |                                                              | Maize Flour and Prodducts (not Koko) |      |                 |
| Millet, guinea corn or sorghum (grain or flour)              | Millet, guinea corn or sorghum (grain or flour)              | Guinea corn/Sorghum                  |      |                 |
|                                                              |                                                              | Millet                               |      |                 |
| Bread or wheat flour                                         | Bread or wheat flour                                         | Bread, buns                          |      |                 |
|                                                              |                                                              | Flour and other cereal products      |      |                 |
| Raw cassava                                                  | Raw cassava                                                  |                                      |      |                 |
| Gari                                                         | Gari                                                         | Gari                                 |      |                 |
| Cassava in a form other than gari (achiekie, tapioca, dough) | Cassava in a form other than gari (achiekie, tapioca, dough) | Cassava                              |      |                 |
|                                                              |                                                              | Cassava dough                        |      |                 |
| Macaroni and Spaghetti                                       | Macaroni and Spaghetti                                       |                                      |      |                 |
| Biscuits and cakes                                           | Biscuits and cakes                                           | Biscuits                             |      |                 |
| Yams                                                         | Yams                                                         | Yam                                  |      |                 |
| Cocoyams                                                     | Cocoyams                                                     | Cocoyams                             |      |                 |
| Plantain                                                     | Plantain                                                     | Plantain                             |      |                 |
|                                                              |                                                              | Other starchy roots and tubers       |      |                 |
|                                                              |                                                              | Kokonte                              |      |                 |
|                                                              |                                                              | Other starchy products               |      |                 |
|                                                              |                                                              | Small beans                          |      |                 |
|                                                              |                                                              | Babara Beans                         |      |                 |
|                                                              |                                                              | Broad Beans                          |      |                 |
| Potato or sweet potato                                       | Potato or sweet potato                                       |                                      |      |                 |
| Kenkey                                                       | Kenkey                                                       |                                      |      |                 |
| Oil palm nuts                                                | Oil palm nuts                                                |                                      |      |                 |
| Ground nuts (roasted, raw or butter)                         | Groundnuts (roasted, raw or butter)                          | Groundnuts                           |      |                 |
|                                                              |                                                              | Other Pulses                         |      |                 |
|                                                              |                                                              | Dawadawa                             |      |                 |
| Coconuts                                                     | Coconuts                                                     |                                      |      |                 |
| Fish and shellfish                                           | Fish and shellfish                                           |                                      |      |                 |
| Chicken                                                      | Chicken                                                      |                                      |      |                 |
| Dove or pigeon                                               | Dove or pigeon                                               |                                      |      |                 |
| Duck                                                         | Duck                                                         |                                      |      |                 |
| Other domestical poultry (turkey, guinea fowl, etc)          | Other domestical poultry (turkey, guinea fowl, etc)          |                                      |      |                 |
| Beef                                                         | Beef                                                         |                                      |      |                 |
| Mutton                                                       | Mutton                                                       |                                      |      |                 |
| Pork                                                         | Pork                                                         |                                      |      |                 |
| Other domesticated meat (goat, etc.)                         | Other domesticated meat (goat, etc)                          |                                      |      |                 |
| Bushmeat and game birds                                      | Bushmeat and game birds                                      |                                      |      |                 |
| Eggs (from all birds)                                        | Eggs (from all birds)                                        |                                      |      |                 |
| Palm oil and shea buter                                      | Palm oil and shea butter                                     |                                      |      |                 |
| Refined oil (gorundnut oil, coconut oil, etc)                | Refined oil (groundnut oil, coconut oil, etc)                |                                      |      |                 |
| Butter, margarine                                            | Butter, margarine                                            |                                      |      |                 |
| Oranges, tangerines                                          | Oranges, tangerines                                          |                                      |      |                 |
| Mangoes                                                      | Mangoes                                                      |                                      |      |                 |
| Pawpaws                                                      | Pawpaws                                                      |                                      |      |                 |
| Avocados                                                     | Avocados                                                     |                                      |      |                 |
| Bananas                                                      | Bananas                                                      |                                      |      |                 |
| Pineapples                                                   | Pineapples                                                   |                                      |      |                 |
| Other fruit (apple, etc.)                                    | Other fruit (apple, etc)                                     |                                      |      |                 |
| Sugar, candy, honey and sugarcane                            | Sugar, candy, honey and sugar cane                           |                                      |      |                 |
| Salt                                                         | Salt                                                         |                                      |      |                 |
| Alcoholic beverages (at home or away from home)              | Alcoholic beverages (at home or away from home)              |                                      |      |                 |
| Non-alcoholic beverages (at home or away form home)          | Non-alcoholic beverages (at home or away from home)          |                                      |      |                 |
| Maggi cubes                                                  | Maggi cubes                                                  |                                      |      |                 |
| Fresh tomatoes                                               | Fresh tomatoes                                               |                                      |      |                 |
| Tomato paste                                                 | Tomato paste                                                 |                                      |      |                 |
| Onions                                                       | Onions                                                       |                                      |      |                 |
| Garden eggs                                                  | Garden Eggs                                                  |                                      |      |                 |
| Okro                                                         | Okro                                                         |                                      |      |                 |
| Bean and peas                                                | Beans and Peas                                               |                                      |      |                 |
| Other non-leafy vegatables (pepper, etc)                     | Other non-leafy vegetables (pepper, etc)                     |                                      |      |                 |
| Cabbage or lettuce                                           | Cabbage or lettuce                                           |                                      |      |                 |
| Spinach or kontomle                                          | Spinach or kontomle                                          |                                      |      |                 |
| Other leafy vegetable (indian spinach, etc )                 | Other leafy vegetable (indian spinach, etc)                  |                                      |      |                 |
| Milk or milk powder                                          | Milk or milk powder                                          |                                      |      |                 |
| Milk products (cheese, yoghurt, etc) No butter               | Milk products (cheese, yoghurt, etc) NOT Butter              |                                      |      |                 |
| Fufu                                                         | Fufu                                                         |                                      |      |                 |
| Tuo zaafi, banku, or akple                                   | Tuo zaafi, banku or akple                                    |                                      |      |                 |
| Emotuo                                                       | Emo tuo                                                      |                                      |      |                 |
| Other prepared foods? (e.g. garrifoto, yor-ke-garri)         | Other prepared foods (eg. garifoto, yor-ke-gari)             |                                      |      |                 |
| Other foods eaten away fro the household (resturants, etc.)  | Other foods eaten away form the household (resturants, etc)  |                                      |      |                 |
| Other foods                                                  | Other foods                                                  |                                      |      |                 |
